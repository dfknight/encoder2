#ifndef __ENGINE_H
#define	__ENGINE_H

// Constants
#define	FALSE	0
#define	TRUE	1

#define MAX_ARGS	9
#define MAX_STRING_LEN	4096

#define	ARG_SRC		"src"
#define	ARG_DST		"dst"
#define	ARG_QUALITY	"quality"
#define ARG_RECURSIVE   "recursive"
#define	ARG_NOSUBS	"no-subs"
#define ARG_LOWPRIORITY "low-priority"
#define ARG_OVERWRITE	"overwrite"
#define ARG_LOG		"log"

#define	QUALITY_LOW	"low"
#define	QUALITY_NORMAL	"normal"
#define	QUALITY_HIGH	"high"

#define SUB_SRT		"srt"
#define	SUB_ASS		"ass"

#define	QUALITY_COMPRESS	18	// Don't preserve quality. May result in noticable loss in quality
#define	QUALITY_REGULAR		20	// Quality loss is minimal.
#define	QUALITY_GOOD		23	// Preserve quality. Quality loss is barely noticable.

// Enums
typedef enum { SRC, DST, QUALITY, RECURSIVE, NOSUBS, LOWPRIORITY, OVERWRITE, LOG, NUM_ARGS } args;
//typedef enum { MKV, AVI, MP4, RMVB, ISO, TS, NUM_EXTENSIONS } extensions;
typedef enum { SUB_NONE, SRT, ASS, NUM_SUBS } subs;

// Main function prototypes
int init_globals(void);
int validate_params(const int argc, const char *args[]);
int preprocess(void);
int encode(void);
int free_globals(void);

#endif
