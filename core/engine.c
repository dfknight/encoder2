#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/resource.h>
#include <sys/timeb.h>

#include "encoder.h"
#include "engine.h"
#include "../util/file.h"
#include "logger.h"

// Global variables
char *valid_args[NUM_ARGS];
char valid_formats[NUM_FORMATS];
char valid_subs[NUM_SUBS];

int quality;
char src[MAX_STRING_LEN];
char dst[MAX_STRING_LEN];
short unsigned int recursive, priority, subtitles, overwrite;

struct EncodeObject;

struct EncodeQueue; // Forward Declaration
struct EncodeQueue *queue;

/*
	Initialize all the global variables.
*/
int init_globals(void) {
	// Allocate memory for the pointers in the array valid_args.
	int x,y;
	for(x=0;x<NUM_ARGS;x++) {
		valid_args[x]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
		// If memory allocation fails, free any memory that
		// was previously allocated before exiting with a
		// FALSE status.
		if(valid_args[x]==NULL) {
			for(y=0;y<x;y++) {
				free(valid_args[x]);
			}
			return FALSE;
		}
	}

	// Initialize the valid_args variable
	strcpy(valid_args[0], ARG_SRC);
	strcpy(valid_args[1], ARG_DST);
	strcpy(valid_args[2], ARG_QUALITY);
	strcpy(valid_args[3], ARG_RECURSIVE);
	strcpy(valid_args[4], ARG_NOSUBS);
	strcpy(valid_args[5], ARG_LOWPRIORITY);
	strcpy(valid_args[6], ARG_OVERWRITE);
	strcpy(valid_args[7], ARG_LOG);

	recursive=FALSE;
	priority=FALSE;
	subtitles=TRUE;
	overwrite=FALSE;

	// Initialize the src and dst strings
	src[0]='\0';
	dst[0]='\0';

	// Initialize the encode queue
	queue=(struct EncodeQueue *)malloc(sizeof(struct EncodeQueue));
	// If memory could not be allocated for the encode queue, the free
	// the memory that was allocated previously before exiting with FALSE.
	if(queue==NULL) {
		for(x=0;x<NUM_ARGS;x++) {
			free(valid_args[x]);
		}
		return FALSE;
	}
	queue->next=NULL;
	queue->object=NULL;

	// Initialize logging global variable
	init_logger();

	return TRUE;
}

/*
	Make sure that all the parameters are valid. Returns TRUE if the function call
	is successful. Otherwise, it will return FALSE.
*/
int validate_params(const int argc, const char *args[]) {
	int x,y;
	char **args_splitted=(char **)malloc(sizeof(char *)*2);
	if(args_splitted==NULL) {
		write_log(LOGTYPE_ERROR, "Unable to allocate memory in validate_params!\n");
		return FALSE;
	}

	for(x=1;x<argc;x++) {
		args_splitted[0]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
		args_splitted[1]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
		// Split argv[x] based on '='
		int count=0;
		if(split_count(args[x], '=', &count) && (count==1 || count==2) && split(args[x], args_splitted, '=')) {
			for(y=0;y<MAX_ARGS;y++) {
				// Compare left side of argv[x] with valid_argv[y]
				if(strcmp(args_splitted[0], valid_args[y])==0) {
					// If right side exists, store it in its appropriate variable
					// If right side does not exist, trigger the flag accordingly
					if(strcmp(args_splitted[0], ARG_QUALITY)==0 && args_splitted[1]!=NULL) {
						if(strcmp(args_splitted[1], QUALITY_LOW)==0) {
							quality=QUALITY_COMPRESS;
						} else if(strcmp(args_splitted[1], QUALITY_NORMAL)==0) {
							quality=QUALITY_REGULAR;
						} else if(strcmp(args_splitted[1], QUALITY_HIGH)==0) {
							quality=QUALITY_GOOD;
						}
					} if(strcmp(args_splitted[0], ARG_SRC)==0 && args_splitted[1]!=NULL) {
						strcpy(src, args_splitted[1]);
					} else if(strcmp(args_splitted[0], ARG_DST)==0 && args_splitted[1]!=NULL) {
						strcpy(dst, args_splitted[1]);
					} else if(strcmp(args_splitted[0], ARG_RECURSIVE)==0) {
						recursive=TRUE;
					} else if(strcmp(args_splitted[0], ARG_LOG)==0) {
						if(get_logtype(args_splitted[1])!=-1) {
							logger_type=get_logtype(args_splitted[1]);
						}
					} else if(strcmp(args_splitted[0], ARG_LOWPRIORITY)==0) {
						priority=TRUE;
					} else if(strcmp(args_splitted[0], ARG_OVERWRITE)==0) {
						overwrite=TRUE;
					} else if(strcmp(args_splitted[0], ARG_NOSUBS)==0) {
						subtitles=FALSE;
					}
					break;
				}
			}
			if(y==MAX_ARGS && strcmp(args_splitted[0], valid_args[y-1])!=0) {
				write_log(LOGTYPE_WARN, "Unknown argument found! (arg='%s')\n", args[x]);
			}
		}
		free(args_splitted[0]);
		free(args_splitted[1]);
	}
	free(args_splitted);

	// Perform checks once the pointers have been freed.
	// This will also avoid memory leaks.
	if(src[0]!='\0') {
		if(!dir_exists(src) && !file_exists(src)) {
			write_log(LOGTYPE_ERROR, "The value of the src parameter is neither file not directory!\n");
			for(x=0;x<MAX_STRING_LEN;x++) {
				src[x]='\0';
				dst[x]='\0';
			}
			return FALSE;
		} else if(dir_exists(src)) {
			write_log(LOGTYPE_INFO, "Source directory exists!\n");
		} else if(file_exists(src)) {
			write_log(LOGTYPE_INFO, "Source file exists!\n");
		}
		replace(src, '\\', '/');
	}

	if(dst[0]!='\0') {
		if(!dir_exists(dst) && !file_exists(dst)) {
			write_log(LOGTYPE_WARN, "The value of the dst parameter is neither file nor directory!\n");
		} else if(dir_exists(dst)) {
			write_log(LOGTYPE_INFO, "Destination directory exists!\n");
		} else if(file_exists(dst)) {
			write_log(LOGTYPE_INFO, "Destination file exists!\n");
		}
		replace(dst, '\\', '/');
	}
	return TRUE;
}

int preprocess(void) {
	printf("TEST\n");
	// Step 1 - Determine if the source and destination are valid.
	if(src[0]=='\0') {
		write_log(LOGTYPE_ERROR, "The source string is empty!\n");
		return FALSE;
	}
	if(dst[0]=='\0') {
		write_log(LOGTYPE_WARN, "The desitnation string is empty!\n");
	}
	write_log(LOGTYPE_DEBUG, "preprocess()\n");

	char temp_src[MAX_STRING_LEN], temp_dst[MAX_STRING_LEN];
	char temp_dir[MAX_STRING_LEN], temp_base[MAX_STRING_LEN];
	char temp_file[MAX_STRING_LEN], temp_format[MAX_STRING_LEN];

	temp_src[0]='\0';
	temp_dst[0]='\0';
	temp_dir[0]='\0';
	temp_base[0]='\0';
	temp_file[0]='\0';
	temp_format[0]='\0';

	strcpy(temp_src, src);
	// Store the directory name, where the source file resides, in temp_dir
	// Store the file name of the source file in temp_base
	if(!dirname(src, temp_dir) || !basename(src, temp_base)) {
		write_log(LOGTYPE_ERROR, "Either dirname or basename failed!\n");
		return FALSE;
	}
	if(dst[0]=='\0') {
		strcpy(temp_dst, temp_dir);
	} else {
		strcpy(temp_dst, dst);
	}
	// Step 2a - If both the src and dst are files, then create a single EcodeObject and add it to the queue
	// Step 2b - If src is a file and dst is a directory, then create a single EncodeObject and add it to the queue
	//           In this case the source and desination file names will be the same, with (possibly) different
	//           file extensions
	if(file_exists(src) && is_valid_format(src)) {
		write_log(LOGTYPE_DEBUG, "preprocess() file_exists(src) && is_valid_format(src)\n");
		struct EncodeObject *object;

		if(dst==NULL || dst[0]=='\0' || !get_file_format_name(dst, temp_format)) {
			strcpy(temp_format, "mp4");
		}
		// Get the filename of the source file and store it in temp_dst
		if(!split_n_z(temp_base, temp_file, '.', 0)) {
			write_log(LOGTYPE_ERROR, "preprocess() filename did not have a format?\n");
			return FALSE;
		}
		// This is for step 2a.
		if(temp_dst[strlen(temp_dst)-1]!='/') {
			strcat(temp_dst, "/");
		}
		if(dst[0]=='\0') {
			strcat(temp_dst, "encodes/");
		}
		strcat(temp_dst, temp_file);
		strcat(temp_dst, ".");
		strcat(temp_dst, temp_format);
		if(	create_encode_object(&object, src, temp_dst, quality) &&
			push_encode_object(queue, object)) {
		}
	}
	// Step 2c - If src and dst are both directories, then get a list of files from the src directory, and create
	//           an EncodeObject for every file in the directory.
	else if(dir_exists(src)) {
		// Find the files in the source directory.
		char **files=NULL;
		int count=0;
		// Step 2c1 - If the recursive switch is on, then scan all the subdirectories as well.
		if((!recursive && get_files(&files, &count, temp_src)) || (recursive && get_files_recursive(&files, &count, temp_src))) {
			int x;
			int count2=0;
			for(x=0;x<count;x++) {
				strcpy(temp_src, files[x]);
				if(!dirname(temp_src, temp_dir) || !basename(temp_src, temp_base)) {
					// Perform error handling here!
				}
				char temp_lastdot[MAX_STRING_LEN];
				strcpy(temp_lastdot, temp_base);
				char *lastdot=strrchr(temp_lastdot, '.');
				*lastdot='\0';
				strcpy(temp_file, temp_lastdot);
				if(!get_file_format_name(temp_src, temp_format) || strcmp(temp_format, FILE_MP4)==0) {
					continue;
				}
				strcpy(temp_format, "mp4");
				if(dst[0]=='\0') {
					strcpy(temp_dst, temp_dir);
					strcat(temp_dst, "/encodes/");
				} else {
					strcpy(temp_dst, dst);
					if(temp_dst[strlen(temp_dst)-1]!='/') {
						strcat(temp_dst, "/");
					}
				}
				strcat(temp_dst, temp_file);
				strcat(temp_dst, ".");
				strcat(temp_dst, temp_format);

				write_log(LOGTYPE_DEBUG, "%s -> %s\n", temp_src, temp_dst);

				struct EncodeObject *object;
				if(create_encode_object(&object, temp_src, temp_dst, quality) && perform_checks(object)) {
					push_encode_object(queue, object);
					count2++;
					// Perform error handling here!
				} else {
					write_log(LOGTYPE_WARN, "Unable to create encode object for file %s\n", temp_src);
					destroy_encode_object(object);
				}
			}
			write_log(LOGTYPE_INFO, "%d/%d files will be encoded!\n", count2, count);
		} else {
			write_log(LOGTYPE_WARN, "files could not be found!\n");
		}

		// For each file in the source directory, create an EncodeObject
	}
	// Step 2d - If src is a directory and dst is a file, return FALSE. This should not be possible.
	// Step 3 - Return true if the EncodeObjects were created successfully, and added to the queue. Otherwise,
	//          return FALSE to signify that this call failed. Also, undo all memory allocation that was done.
	return TRUE;
}

int encode(void) {
	//print_encode_queue(queue);
	write_log(LOGTYPE_DEBUG, "encode()\n");
	if(queue==NULL || queue->object==NULL) {
		write_log(LOGTYPE_WARN, "Empty queue received. Nothing to encode!\n");
		return FALSE;
	}
	while(queue!=NULL && queue->object!=NULL) {
		struct EncodeObject *object=NULL;
		char dest_dir[MAX_STRING_LEN];
		char **exec_args=NULL;
		char ch[2], child_output[MAX_STRING_LEN];
		int x, num_args=10;
		short unsigned int src_time, src_total_frames;
		float src_fps;
		struct timeb start, end, current;
		int diff;
		write_log(LOGTYPE_DEBUG, "Popping encode object!\n");
		if(!pop_encode_object(&queue, &object)) {
			write_log(LOGTYPE_ERROR, "Unable to pop encode queue!\n");
			return FALSE;
		} else if(!prepare_args(&exec_args, object)) {
			write_log(LOGTYPE_ERROR, "Unable to prepare args!\n");
			destroy_encode_object(object);
			continue;
		}
		write_log(LOGTYPE_DEBUG, "Done!\n");

		if(!dirname(object->dst, dest_dir)) {
			write_log(LOGTYPE_ERROR, "Unable to get dirname to store encoded file!\n");
			destroy_encode_object(object);
			continue;
		} else if(!dir_exists(dest_dir) && !create_dir(dest_dir)) {
			write_log(LOGTYPE_ERROR, "Destination directory (%s) doesn't exist and could not be created!\n", dest_dir);
			destroy_encode_object(object);
			continue;
		}
		if(subtitles && subs_exist(object->src)) {
			num_args+=2;
		}

		write_log(LOGTYPE_INFO, "Start encoding %s\n", object->src);

		get_fps(object->src, &src_fps);
		get_duration(object->src, &src_time);
		src_total_frames=src_fps*src_time;

		pid_t child;
		int pipe_fd[2];

		pipe(pipe_fd);
		child=vfork();

		if(child==0) {
			// In the child
			close(pipe_fd[0]);
			if(dup2(pipe_fd[1], STDERR_FILENO)<0) {
				write_log(LOGTYPE_ERROR, "dup2 failed!\n");
				exit(EXIT_FAILURE);
			}
			execvp("bin/ffmpeg", exec_args);
			write_log(LOGTYPE_ERROR, "Call to execve failed!\n");
			exit(EXIT_FAILURE);
		} else if(child>0) {
			// In the parent
			close(pipe_fd[1]);
			int status=-1;
			if(priority) {
				setpriority(PRIO_PROCESS, child, 19);
			}
			ftime(&start);
			while(	waitpid(child, &status, WNOHANG)==0 || strlen(child_output)>0) {
				if(strlen(child_output)<MAX_STRING_LEN && read(pipe_fd[0], ch, 1)==1) {
						ch[1]='\0';
						strcat(child_output, ch);
					}
					if(ch[0]=='\n') {
						child_output[0]='\0';
					}
					if(ch[0]=='\r') {
						if(contains_str(child_output, "frame") && contains_str(child_output, "size") && contains_str(child_output, "time")) {
							char fps_full_str[MAX_STRING_LEN], fps_str[MAX_STRING_LEN];
							char frame_full_str[MAX_STRING_LEN], frame_str[MAX_STRING_LEN];
							int cur_fps=0, cur_frame=0;
							if(split_n_z(child_output, fps_full_str, '=', 2) && split_n_z(fps_full_str, fps_str, ' ', 0)) {
								cur_fps=atoi(fps_str);
							} else {
								write_log(LOGTYPE_INFO, "fps parsing failed!\n");
							}
							if(split_n_z(child_output, frame_full_str, '=', 1) && split_n_z(frame_full_str, frame_str, ' ', 0)) {
								cur_frame=atoi(frame_str);
							} else {
								write_log(LOGTYPE_INFO, "frame parsing failed!\n");
							}
							if(cur_fps>0) {
								int cur_time[3], remaining_frames=0;
								char output[MAX_STRING_LEN];
								int time_elapsed=0, elap_time[3];

								remaining_frames=src_total_frames-cur_frame;

								object->status.eta=(short unsigned int)(remaining_frames/cur_fps);
								object->status.progress=(float)(cur_frame)/src_total_frames*100;
								if(object->status.progress>100) {
									object->status.progress=100.0f;
								}

								cur_time[0]=object->status.eta/(60*60);
								cur_time[1]=(object->status.eta-cur_time[0]*60*60)/60;
								cur_time[2]=object->status.eta-cur_time[0]*60*60-cur_time[1]*60;

								ftime(&current);
								time_elapsed=current.time-start.time;
								elap_time[0]=time_elapsed/(60*60);
								elap_time[1]=(time_elapsed-elap_time[0]*60*60)/60;
								elap_time[2]=time_elapsed-elap_time[0]*60*60-elap_time[1]*60;

								sprintf(output, "Progress: %.2f%%, Time Elapsed: %.2d:%.2d:%.2d, Time Remaining: %.2d:%.2d:%.2d", object->status.progress,
									elap_time[0], elap_time[1], elap_time[2], cur_time[0], cur_time[1], cur_time[2]);
								while(strlen(output)<30) {
									strcat(output, " ");
								}
								write_log(LOGTYPE_INFO, "%s\r", output);
							}
							// Parse the output here to display only the time remaining and progress
						}
						//child_output[0]='\0';
						memset(child_output, '\0', MAX_STRING_LEN);
					}
				}
				ftime(&end);
				close(pipe_fd[0]);
				sleep(5);
				if(overwrite) {
					char dst_filename[MAX_STRING_LEN], dst_basename[MAX_STRING_LEN];
					if(dirname(object->src, dst_filename) && basename(object->dst, dst_basename)) {
						strcat(dst_filename, "/");
						strcat(dst_filename, dst_basename);
						if(!move_file(object->dst, dst_filename)) {
							write_log(LOGTYPE_WARN, "File overwite failed!\n");
						} else if(remove(object->src)!=0) {
							write_log(LOGTYPE_WARN, "Source file could not be deleted!\n");
						} else {
							write_log(LOGTYPE_INFO, "File moved from %s to %s\n", object->dst, dst_filename);
						}
					}
				}
				write_log(LOGTYPE_INFO, "\n\nEncode is DONE!\n");
			} else {
				write_log(LOGTYPE_ERROR, "Unable to fork a child process!\n");
			}
		//#endif
		for(x=0;x<num_args;x++) {
			free(exec_args[x]);
		}
		free(exec_args);

		destroy_encode_object(object);
	}
	return TRUE;
}

/*
	Free the memory allocated by each of the global variables.
*/
int free_globals(void) {
	if(queue!=NULL) {
		int x;
		struct EncodeQueue *temp = queue;
		for(x=0;temp->next!=NULL;x++) {
			temp=temp->next;
			free(queue);
			queue=temp;
		}
	}
	return TRUE;
}

int perform_checks(const struct EncodeObject *object) {
	if(object==NULL) {
		write_log(LOGTYPE_WARN, "NULL object received!\n");
		return FALSE;
	}

	float src_fps;
	short unsigned int src_duration=0, dst_duration=0;
	if(file_exists(object->src)) {
		if(!get_duration(object->src, &src_duration)) {
			write_log(LOGTYPE_WARN, "Unable to obtain duration from source file!\n", src_duration);
			return FALSE;
		}
		if(!get_fps(object->src, &src_fps)) {
			write_log(LOGTYPE_WARN, "Unable to obtain fps from source file!\n", src_fps);
			return FALSE;
		}
	}
	if(file_exists(object->dst)) {
		if(!get_duration(object->dst, &dst_duration)) {
			//write_log(LOGTYPE_WARN, "Unable to obtain duration from destination file %s!\n", object->dst);
		}
		if(abs(src_duration-dst_duration)<5) {
			write_log(LOGTYPE_WARN, "Destination duration is close to source duration! (src=%d, dst=%d)\n", src_duration, dst_duration);
			return FALSE;
		}
	}
	// Check to see if difference between the duration of the source and destination files is less than 5.
	// If it is, then return FALSE.
	return TRUE;
}

int get_num_args(const struct EncodeObject *object) {
	int num_args=12;
	if(priority) {
		num_args+=2;
	}
	if(subtitles && subs_exist(object->src)) {
		num_args+=3;
	}
	return num_args;
}

int prepare_args(char ***args, const struct EncodeObject *object) {
	if(object==NULL) {
		return FALSE;
	}
	int num_args=get_num_args(object), counter=0;
	if(*args==NULL) {
		(*args)=(char **)malloc(sizeof(char *)*num_args);
		int x;
		for(x=0;x<num_args;x++) {
			(*args)[x]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
		}
	}
	char subtitle_src[MAX_STRING_LEN];
	strcpy(subtitle_src, object->src);
	clean_str(subtitle_src);
	strcpy((*args)[counter++], "bin/ffmpeg");
	strcpy((*args)[counter++], "-i");
	strcpy((*args)[counter++], object->src);
	strcpy((*args)[counter++], "-c:v");
	strcpy((*args)[counter++], "libx264");
	strcpy((*args)[counter++], "-c:a:0");
	strcpy((*args)[counter++], "copy");
	// Consider switching to -qp
	strcpy((*args)[counter++], "-crf");
	switch(object->quality) {
		case QUALITY_GOOD:
			strcpy((*args)[counter++], "18");
			break;
		case QUALITY_COMPRESS:
			strcpy((*args)[counter++], "23");
			break;
		case QUALITY_REGULAR:
		default:
			strcpy((*args)[counter++], "20");
			break;
	}
	if(priority) {
		int num_cores = get_nprocs();
		char num_cores_str[3];
		snprintf(num_cores_str, 2, "%d", num_cores-1);
		strcpy((*args)[counter++], "-threads");
		strcpy((*args)[counter++], num_cores_str);
	}
	if(subtitles && subs_exist(object->src)) {
		strcpy((*args)[counter++], "-map_metadata=1");
		strcpy((*args)[counter++], "-vf");
		strcpy((*args)[counter], "subtitles='");
		strcat((*args)[counter], subtitle_src);
		strcat((*args)[counter++], "'");
	}
	strcpy((*args)[counter++], "-y");
	strcpy((*args)[counter++], object->dst);
	(*args)[counter++]=NULL;

	return TRUE;
}
