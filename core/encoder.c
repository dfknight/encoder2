#ifdef __MINGW32__
	#include <direct.h>
#endif

#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <sys/types.h>
#include <sys/stat.h>

#include "encoder.h"
//#include "engine.h"
#include "../util/file.h"
#include "logger.h"

/*
	Create an instance of EncodeObject with the specified values.
*/
int create_encode_object(struct EncodeObject **object, const char *src, const char *dst, const int quality) {
	if(src!=NULL && src[0]!='\0' && dst!=NULL && dst[0]!='\0' && quality > 0 && quality < 30) {
		*object = (struct EncodeObject *)malloc(sizeof(struct EncodeObject));
		if(object==NULL) {
			return FALSE;
		}
		strcpy((*object)->src, src);
		strcpy((*object)->dst, dst);
		(*object)->quality=quality;
		(*object)->status.eta=0;
		(*object)->status.progress=0.0f;
		return TRUE;
	}
	return FALSE;
}

/*
	Push an EncodeObject to the end of the specified EncodeQueue.
*/
int push_encode_object(struct EncodeQueue *queue, struct EncodeObject *object) {
	if(object!=NULL) {
		struct EncodeQueue *temp_queue=queue;
		if(temp_queue==NULL) {
			queue=(struct EncodeQueue *)malloc(sizeof(struct EncodeQueue));
			temp_queue=queue;
		} else if(temp_queue->object!=NULL) {
			while(temp_queue->next!=NULL) {
				temp_queue=temp_queue->next;
			}
			temp_queue->next=(struct EncodeQueue *)malloc(sizeof(struct EncodeQueue));
			temp_queue=temp_queue->next;
		}
		temp_queue->object=object;
		temp_queue->next=NULL;
		return TRUE;
	}
	return FALSE;
}

/*
	Remove an EncodeObject from the beginning of the specified EncodeQueue.
*/
int pop_encode_object(struct EncodeQueue **queue, struct EncodeObject **object){
	if(*queue!=NULL && (*queue)->object!=NULL) {
		if(*object==NULL) {
			*object=(*queue)->object;
			struct EncodeQueue *temp=*queue;
			if((*queue)->next!=NULL) {
				*queue=(*queue)->next;
			} else {
				*queue=NULL;
			}
			free(temp);
			return TRUE;
		}
		write_log(LOGTYPE_WARN, "Object not found in queue!");
	} else {
		write_log(LOGTYPE_WARN, "No EncodeObject found in encode queue!");
	}
	return FALSE;

}

/*
	Destroy the EncodeObject.
	Returns TRUE if the memory allocated by the EncodeObject was freed, or FALSE
	if the object has not been allocated any memory.
*/
int destroy_encode_object(struct EncodeObject *object) {
	write_log(LOGTYPE_INFO, "destroy_encode_object()\n");
	if(object==NULL) {
		return FALSE;
	}
	free(object);
	return TRUE;
}

void print_encode_queue(struct EncodeQueue *queue) {
	int counter=0;
	write_log(LOGTYPE_INFO, "print_encode_queue()\n");
	if(queue!=NULL && queue->object!=NULL) {
		struct EncodeQueue *temp=queue;
		while(temp!=NULL) {
			write_log(LOGTYPE_INFO, "#%d - Source: %s\n", counter+1, temp->object->src);
			write_log(LOGTYPE_INFO, "#%d - Destination: %s\n", counter+1, temp->object->dst);
			temp=temp->next;
			counter++;
		}
	}
	if(counter==0) {
		write_log(LOGTYPE_INFO, "queue is empty!");
	}
}
