#ifndef __LOGGER_H
#define __LOGGER_H

enum LogNum { LOGTYPE_NONE, LOGTYPE_ERROR, LOGTYPE_INFO, LOGTYPE_WARN, LOGTYPE_DEBUG };

typedef enum LogNum LogType;

extern LogType logger_type;
extern int logger_write_to_file;

int init_logger();
int write_log(LogType message_type, const char *message, ...);
LogType get_logtype(const char *type_str);
int get_logtype_str(LogType type);

#endif
