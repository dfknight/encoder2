#ifndef __ENCODER_H
#define	__ENCODER_H

#include "engine.h"

//#define	MAX_STRING_LEN	4096

// Structures
struct EncodeStatus {
	short unsigned int eta;
	float progress;
};

struct EncodeObject {
        char src[MAX_STRING_LEN];
        char dst[MAX_STRING_LEN];
        unsigned short quality;

        char sub[MAX_STRING_LEN];
        char subs_file[MAX_STRING_LEN];

	struct EncodeStatus status;
};

// The EncodeQueue structure is a Linked List data structure
// that points to a list of EncodeObject structures that are
// ready to be encoded.
struct EncodeQueue {
	struct EncodeObject *object;
	struct EncodeQueue *next;
};

// EncodeQueue and EncodeObject functions
int create_encode_object(struct EncodeObject **object, const char *src, const char *dst, const int quality);
int push_encode_object(struct EncodeQueue *queue, struct EncodeObject *object);
int pop_encode_object(struct EncodeQueue **queue, struct EncodeObject **object);
int destroy_encode_object(struct EncodeObject *object);
void print_encode_queue(struct EncodeQueue *queue);

#endif
