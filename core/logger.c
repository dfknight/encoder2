#include <stdarg.h>
#include <stdio.h>
#include <string.h>

#include "engine.h"
#include "logger.h"

LogType logger_type;
int logger_write_to_file;

int init_logger() {
	logger_type=LOGTYPE_NONE;
	logger_write_to_file=FALSE;
}

int write_log(LogType message_type, const char *message, ...) {
	if(message_type<=logger_type && message_type!=LOGTYPE_NONE) {
		char string[MAX_STRING_LEN];
		int x;

		va_list v1;
		va_start(v1, message);
		strcpy(string, "");

		for(x=0;x<strlen(message);x++) {
			char temp[MAX_STRING_LEN];
			if(message[x]=='%') {
				if(message[x+1]=='s') {
					sprintf(temp, "%s", va_arg(v1, const char *));
				} else if(message[x+1]=='d') {
					sprintf(temp, "%d", va_arg(v1, int));
				} else if(message[x+1]=='f') {
					sprintf(temp, "%f", va_arg(v1, double));
				} else if(message[x+1]=='c') {
					sprintf(temp, "%c", va_arg(v1, int));
				} else if(message[x+1]=='%') {
					strcpy(temp, "%");
				}
				x++;
			} else {
				sprintf(temp, "%c", message[x]);
			}
			strcat(string, temp);
		}

		switch(message_type) {
			case LOGTYPE_DEBUG:
				fprintf(stdout, "DEBUG: %s", string);
				break;
			case LOGTYPE_WARN:
				fprintf(stdout, "WARNING: %s", string);
				break;
			case LOGTYPE_INFO:
				fprintf(stdout, "INFO: %s", string);
				break;
			case LOGTYPE_ERROR:
				fprintf(stdout, "ERROR: %s", string);
				break;
			default:
				return FALSE;
		}
		fflush(stdout);
		strcpy(string, "");
		return TRUE;
	}
	return FALSE;
}

/*
	Returns either a valid LogType or -1.
*/
LogType get_logtype(const char *message) {
	if(message!=NULL) {
		char temp_message[MAX_STRING_LEN];
		strcpy(temp_message, message);
		if(to_upper(temp_message)) {
			if(strcmp(temp_message, "NONE")==0) {
				return LOGTYPE_NONE;
			} else if(strcmp(temp_message, "INFO")==0) {
				return LOGTYPE_INFO;
			} else if(strcmp(temp_message, "WARN")==0) {
				return LOGTYPE_WARN;
			} else if(strcmp(temp_message, "ERROR")==0) {
				return LOGTYPE_ERROR;
			} else if(strcmp(temp_message, "DEBUG")==0) {
				return LOGTYPE_DEBUG;
			}
		}
	}
	return -1;
}
