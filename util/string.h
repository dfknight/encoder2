#ifndef STRING_H
#define STRING_H

int split(const char *source, char **destination, const char delimiter);
int split_n(const char *source, char *destination, const char delimiter, int num);
int rsplit(const char *source, char **destination, const char delimiter);
int rsplit_n(const char *source, char *destination, const char *delimiter, int num);
int split_count(const char *source, const char delimiter, int *count);

int split_n_z(const char *source, char *destination, const char delimiter, int num);
int rsplit_n_z(const char *source, char *destination, const char delimiter, int num);

int replace (char *string, const char from, const char to);

int contains(const char *string, const char ch);
int contains_str(const char *source, const char *string);

int to_lower(char *string);
int to_upper(char *string);

int clean_sub_str(char *string);
int clean_str(char *string);

#endif
