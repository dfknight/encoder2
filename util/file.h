#ifndef __FILE_H
#define __FILE_H

#define	FILE_MKV	"mkv"
#define	FILE_MP4	"mp4"
#define	FILE_AVI	"avi"
#define	FILE_RMVB	"rmvb"
#define	FILE_ISO	"iso"
#define	FILE_TS		"video_ts"

typedef enum {
	FORMAT_MKV,
	FORMAT_AVI,
	FORMAT_MP4,
	FORMAT_RMVB,
	FORMAT_ISO,
	FORMAT_TS,
	NUM_FORMATS
} formats;

int create_dir(const char *dirname);
int move_file(const char *source, const char *destination);

int file_exists(const char *filename);
int dir_exists(const char *dirname);

int get_num_files(const char *dirname);
int get_num_files_recursive(const char *dirname);

int get_files(char ***files, int *count, const char *dirname);
int get_files_recursive(char ***files, int *count, const char *dirname);

int get_file_format(const char *filename);
int get_file_format_name(const char *filename, char *format);

int strtolower(char *dst, const char *src);

int is_valid_format(const char *filename);

int basename(const char *path, char *basename);
int dirname(const char *path, char *dirname);

// Video files prototypes
int get_duration(const char *filename, short unsigned int *duration);
int get_fps(const char *filename, float *fps);

int subs_exist(const char *filename);

#endif
