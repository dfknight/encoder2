#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../core/engine.h"
#include "string.h"

/*
	Split the contents of the source into an array of strings based on the
	delimiter provided. The function will return TRUE if the operation was
	successful, or FALSE if the source or destination are NULL.
*/
int split(const char *source, char **destination, const char delimiter) {
	if(source==NULL || destination==NULL) {
		return FALSE;
	}
	char temp[strlen(source)], delimiter_str[2];
	strcpy(temp, source);

	delimiter_str[0]=delimiter;
	delimiter_str[1]='\0';

	char *temp_pointer=strtok(temp, delimiter_str);
	int count=0;
	while(temp_pointer) {
		strcpy(destination[count++], temp_pointer);
		temp_pointer=strtok(NULL, delimiter_str);
	}
	return TRUE;
}
/*
	Split the contents of the source into an array of strings based on the
	delimiter provided. The n-th element in the array is stored in destination.
	This function will return TRUE if the operation was succesful, or FALSE
	if the source or destination pointers are NULL.
	NOTE: 	This function is only here as a placeholder.
		Remove this upon the implementation of the split_n function.
*/
int split_n_z(const char *source, char *destination, const char delimiter, int num) {
	if(source==NULL || destination==NULL) {
		return FALSE;
	}
	char temp[strlen(source)], delimiter_str[2];
	strcpy(temp, source);

	delimiter_str[0]=delimiter;
	delimiter_str[1]='\0';

	char *temp_pointer=strtok(temp, delimiter_str);
	int count=0;
	while(temp_pointer && count<num) {
		temp_pointer=strtok(NULL, delimiter_str);
		count++;
	}
	if(temp_pointer!=NULL) {
		strcpy(destination, temp_pointer);
	}
	return TRUE;
}

int rsplit_n_z(const char *source, char *destination, const char delimiter, int num) {
	if(source==NULL || destination==NULL) {
		return FALSE;
	}
	char temp[strlen(source)], delimiter_str[2];
	strcpy(temp, source);

	delimiter_str[0]=delimiter;
	delimiter_str[1]='\0';

	char *temp_pointer=strtok(temp, delimiter_str);
	int count=0;
	while(temp_pointer) {
		temp_pointer=strtok(NULL, delimiter_str);
		count++;
	}

	strcpy(temp, source);
	temp_pointer=strtok(temp, delimiter_str);
	int count2=0;
	while(temp_pointer && count2!=(count-num-1)) {
		temp_pointer=strtok(NULL, delimiter_str);
		count2++;
	}

	if(temp_pointer!=NULL) {
		strcpy(destination, temp_pointer);
	}
	return TRUE;
}

int rsplit(const char *source, char **destination, const char delimiter) {
	if(source==NULL || destination==NULL) {
		return FALSE;
	}
	return TRUE;
}
int rsplit_n(const char *source, char *destination, const char *delimiter, int num) {
	if(source==NULL || destination==NULL) {
		return FALSE;
	}
	return TRUE;
}
int split_count(const char *source, const char delimiter, int *count) {
	if(source==NULL || count==NULL) {
		return FALSE;
	}
	char temp[strlen(source)], delimiter_str[2];
	strcpy(temp, source);

	delimiter_str[0]=delimiter;
	delimiter_str[1]='\0';
	*count=0;

	char *temp_pointer=strtok(temp, delimiter_str);
	while(temp_pointer) {
		(*count)++;
		temp_pointer=strtok(NULL, delimiter_str);
	}
	if(*count==0) {
		return FALSE;
	}
	return TRUE;
}
int replace(char *string, const char from, const char to) {
	if(string!=NULL){
		int x;
		for(x=0;x<strlen(string);x++) {
			if(string[x]==from) {
				string[x]=to;
			}
		}
		if(x==strlen(string)) {
			return TRUE;
		}
	}
	return FALSE;
}
int contains(const char *string, const char ch) {
	if(string!=NULL) {
		int x;
		for(x=0;x<strlen(string);x++) {
			if(string[x]==ch) {
				return TRUE;
			}
		}
	}
	return FALSE;
}

int contains_str(const char *source, const char *string) {
	if(string!=NULL && source!=NULL) {
		char temp[MAX_STRING_LEN];
		strcpy(temp, source);
		if(strstr(temp, string)!=NULL) {
			return TRUE;
		}
	}
	return FALSE;
}

/*
	Convert the string to a lower case string.
	Returns TRUE if the string was changed, or FALSE if the string
	provided was NULL.
*/
int to_lower(char *string) {
	if(string==NULL) {
		return FALSE;
	}

	int x;
	for(x=0;x<strlen(string);x++) {
		string[x]=tolower(string[x]);
	}
	return TRUE;
}
/*
	Convert the string to an upper case string.
	Returns TRUE if the string was changed, or FALSE if the string
	provided was NULL.
*/
int to_upper(char *string) {
	if(string==NULL) {
		return FALSE;
	}
	int x;
	for(x=0;x<strlen(string);x++) {
		string[x]=toupper(string[x]);
	}
	return TRUE;
}

int clean_str(char *string) {
	if(string==NULL) {
		return FALSE;
	}
	int x;
	char temp[MAX_STRING_LEN];
	temp[0]='\0';
	for(x=0;x<strlen(string);x++) {
		if(string[x]==' '||string[x]=='['||string[x]==']') {
			char temp2[2];
			temp2[0]=string[x];
			temp2[1]='\0';
			strcat(temp, "\\");
			strcat(temp, temp2);
		} else {
			char temp2[2];
			temp2[0]=string[x];
			temp2[1]='\0';
			strcat(temp, temp2);
		}
	}
	strcpy(string, temp);
	return TRUE;
}

int clean_sub_str(char *string) {
	if(string==NULL) {
		return FALSE;
	}
	int x;
	char temp[MAX_STRING_LEN];
	temp[0]='\0';
	for(x=0;x<strlen(string);x++) {
		if(string[x]==':') {
			strcat(temp, "\\\\:");
		} else if(string[x]=='\\') {
			strcat(temp, "\\\\\\\\");
		} else if(string[x]=='[' || string[x]==']') {
			char temp2[2];
			temp2[0]=string[x];
			temp2[1]='\0';
			strcat(temp, "\\");
			strcat(temp, temp2);
		} else {
			char temp2[2];
			temp2[0]=string[x];
			temp2[1]='\0';
			strcat(temp, temp2);
		}
	}
	strcpy(string, temp);
	return TRUE;
}
