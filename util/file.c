#include <dirent.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/stat.h>

#include "../core/engine.h"
#include "../core/logger.h"
#include "file.h"

/*
	Create a directory.
*/
int create_dir(const char *dirname) {
	if(!dir_exists(dirname)) {
		int result=-1;
		result=mkdir(dirname, 0755);
		if(result==0) {
			return TRUE;
		}
	}
	return FALSE;
}

int move_file(const char *source, const char *destination) {
	if(file_exists(source)) {
		if(rename(source, destination)==0) {
			return TRUE;
		} else {
			// Use mv if rename fails.
			// This will only happen if the source/destination are on different file systems (network/different drives).
			char command[MAX_STRING_LEN];
			command[0]='\0';
			strcat(command, "mv \"");
			strcat(command, source);
			strcat(command, "\" \"");
			strcat(command, destination);
			strcat(command, "\"");
			FILE *p=popen(command, "r");
			pclose(p);
			return TRUE;
		}
	}
	return FALSE;
}

/*
	Check to see if the specified file exists.
*/
int file_exists(const char *filename) {
	struct stat info;
	if(stat(filename, &info)==0 && info.st_mode & S_IFREG) {
		return TRUE;
	}
	return FALSE;
}

/*
	Check to see if the specified directory exists or not.
*/
int dir_exists(const char *filename) {
	struct stat info;
	if((filename[1]==':' && (filename[2]=='\\' || filename[2]=='/')) || (stat(filename, &info)==0 && info.st_mode & S_IFDIR)) {
		return TRUE;
	}
	return FALSE;
}

/*
	Get the number of files in the specified directory.
*/
int get_num_files(const char *dirname) {
	int count=0;
	struct dirent *dir;
	DIR *d=opendir(dirname);
	if(d) {
		while((dir=readdir(d))!=NULL) {
			char temp[MAX_STRING_LEN];
			strcpy(temp, dirname);
			if(temp[strlen(temp)-1]!='/') {
				strcat(temp, "/");
			}
			strcat(temp, dir->d_name);
			struct stat filestat;
			if(stat(temp, &filestat)!=-1 && S_ISREG(filestat.st_mode)) {
				count++;
			}
		}
		closedir(d);
	}
	return count;
}

/*
	Get the number of files in the specified directory (recursively).
*/
int get_num_files_recursive(const char *dirname) {
	int count=0;
	struct dirent *dir;

	DIR *d;
	d=opendir(dirname);
	if(d) {
		while((dir=readdir(d))!=NULL) {
			char temp[MAX_STRING_LEN];
			strcpy(temp, dirname);
			if(temp[strlen(temp)-1]!='/') {
				strcat(temp, "/");
			}
			strcat(temp, dir->d_name);
			struct stat filestat;
			if(stat(temp, &filestat)!=-1 && S_ISREG(filestat.st_mode)) {
				count++;
			} else if(S_ISDIR(filestat.st_mode) && dir->d_name[0]!='.') {
				count+=get_num_files_recursive(temp);
			}
		}
	}
	closedir(d);

	return count;
}

/*
	Get all the file names in the provided directory. Returns TRUE if the operation
	was successful, or FALSE if it failed.
*/
int get_files(char ***files, int *count, const char *dirname) {
	if(dirname==NULL || count==NULL) {
		return FALSE;
	}
	*count=get_num_files(dirname);
	struct dirent *dir;
	DIR *d=opendir(dirname);
	if(*files==NULL) {
		*files=(char **)malloc(sizeof(char *)* *count);
		int x;
		for(x=0;x<*count;x++) {
			(*files)[x]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
		}
	}
	if(d) {
		int counter=0;
		while((dir=readdir(d))!=NULL) {
			char temp[MAX_STRING_LEN];
			strcpy(temp, dirname);
			if(temp[strlen(temp)-1]!='/') {
				strcat(temp, "/");
			}
			strcat(temp, dir->d_name);
			struct stat filestat;
			if(stat(temp, &filestat)!=-1 && S_ISREG(filestat.st_mode)) {
				strcpy((*files)[counter++], temp);
			}
		}
	}
	return TRUE;
}

/*
	Recursively iterate through the specified directory and store the file names
	of each file into the files array. This will return TRUE if successful, or FALSE
	if it fails.
	NOTE: count should be initialized to 0 for accurate results.
*/
int get_files_recursive(char ***files, int *count, const char *dirname) {
	if(dirname==NULL || count==NULL) {
		return FALSE;
	}
	if(*count==0) {
		*count=get_num_files_recursive(dirname);
	}
	struct dirent *dir;
	DIR *d=opendir(dirname);

	if(*files==NULL) {
		*files=(char **)malloc(sizeof(char *)* *count);
		int x;
		for(x=0;x<*count;x++) {
			(*files)[x]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
			(*files)[x][0]='\0';
		}
	}

	if(d) {
		int counter=0;
		while(counter<*count && (*files)[counter][0]!='\0') {
			counter++;
		}
		while((dir=readdir(d))!=NULL) {
			char temp[MAX_STRING_LEN];
			strcpy(temp, dirname);
			if(temp[strlen(temp)-1]!='/') {
				strcat(temp, "/");
			}
			strcat(temp, dir->d_name);
			struct stat filestat;
			if(stat(temp, &filestat)!=-1) {
				if(S_ISREG(filestat.st_mode)) {
					strcpy((*files)[counter++], temp);
				} else if(S_ISDIR(filestat.st_mode) && dir->d_name[0]!='.') {
					if(!get_files_recursive(files, count, temp)) {
						int x;
						for(x=0;x<*count;x++) {
							free((*files)[x]);
						}
						free(*files);
						*count=0;
						return FALSE;
					} else {
						while(counter<*count && (*files)[counter][0]!='\0') {
							counter++;
						}
					}
				}
			}
		}
		closedir(d);
		return TRUE;
	}
	return FALSE;
}

/*
	Get the format of the file. Returns FALSE if the format is not supported by
	this application, otherwise this function will return a valid format represented
	as an int.
*/
int get_file_format(const char *filename) {
	struct stat info;
	if(stat(filename, &info)==0 && info.st_mode & S_IFREG) {
		// rsplit based on delimiter = '.'
		const char *temp=strrchr(filename, '.');
		if(temp!=NULL && temp!=filename) {
			if(strcmp(temp, FILE_MKV)==0) {
				return FORMAT_MKV;
			} else if(strcmp(temp, FILE_MP4)==0) {
				return FORMAT_MP4;
			} else if(strcmp(temp, FILE_AVI)==0) {
				return FORMAT_AVI;
			} else if(strcmp(temp, FILE_RMVB)==0) {
				return FORMAT_RMVB;
			} else if(strcmp(temp, FILE_TS)==0) {
				return FORMAT_TS;
			} else if(strcmp(temp, FILE_ISO)==0) {
				return FORMAT_ISO;
			}
		}
	}
	return FALSE;;
}

/*
	If the specified file has a valid format that is supported by this application,
	this function will return TRUE if the format is valid, otherwise it will return
	FALSE.
*/
int get_file_format_name(const char *filename, char *format) {
	char temp[MAX_STRING_LEN], temp_filename[MAX_STRING_LEN];
	strcpy(temp_filename, filename);
	if(strrchr(temp_filename, '.')==NULL) {
		return FALSE;
	}
	strcpy(temp_filename, filename);
	strcpy(temp, strrchr(temp_filename, '.'));
	if(temp[0]=='.') {
		memmove(temp, temp+1, strlen(temp));
		if(	strcmp(temp, FILE_MKV)==0 || strcmp(temp, FILE_MP4)==0 ||
			strcmp(temp, FILE_AVI)==0 || strcmp(temp, FILE_RMVB)==0 ||
			strcmp(temp, FILE_TS)==0 || strcmp(temp, FILE_ISO)==0) {
			strcpy(format, temp);
			return TRUE;
		}
	}
	return FALSE;
}

/*
	This function is used to conver upper case characters in a string to lower case.
	The result is stored in the parameter called dst. If the function is successful,
	then the result stored in dst is considered to be valid. Otherwise, the data stored
	in dst should be disregarded, as it may be invalid.

	Returns TRUE upon success, and FALSE otherwise.
*/
int strtolower(char *dst, const char *src) {
	if(src!=NULL && dst!=NULL) {
		int x;
		for(x=0;x<strlen(src)+1;x++) {
			if(src[x]>=65 && src[x]<=90) {
				dst[x]=src[x]+32;
			} else {
				dst[x]=src[x];
			}
		}
		return TRUE;
	}
	return FALSE;
}

int is_valid_format(const char *filename) {
	char format[MAX_STRING_LEN];
	get_file_format_name(filename, format);
	if( get_file_format_name(filename, format)==TRUE &&
	    (	strcmp(format, FILE_MKV)==0 || strcmp(format, FILE_AVI)==0 || strcmp(format, FILE_MP4)==0 ||
		strcmp(format, FILE_RMVB)==0 || strcmp(format, FILE_ISO)==0 )){
		return TRUE;
	}
	return FALSE;
}

int basename(const char *path, char *basename) {
	if(path==NULL || path[0]=='\0') {
		return FALSE;
	}

	int num_splits;
	char temp_path[MAX_STRING_LEN];
	strcpy(temp_path, path);
	if(!split_count(temp_path, '/', &num_splits)) {
		return FALSE;
	}

	int x;
	char **temp_path_split=(char **)malloc(sizeof(char *)*num_splits);
	for(x=0;x<num_splits;x++) {
		temp_path_split[x]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
	}

	if(!split(temp_path, temp_path_split, '/')) {
		return FALSE;
	}
	strcpy(basename, temp_path_split[num_splits-1]);

	for(x=0;x<num_splits;x++) {
		free(temp_path_split[x]);
	}
	free(temp_path_split);
	return TRUE;
}

int dirname(const char *path, char *dirname) {
	if(path==NULL || path[0]=='\0') {
		return FALSE;
	}

	int num_splits;
	char temp_path[MAX_STRING_LEN];
	strcpy(temp_path, path);
	if(!split_count(temp_path, '/', &num_splits)) {
		return FALSE;
	}

	int x;
	char **temp_path_split=(char **)malloc(sizeof(char *)*num_splits);
	for(x=0;x<num_splits;x++) {
		temp_path_split[x]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
	}

	if(!split(temp_path, temp_path_split, '/')) {
		return FALSE;
	}

	if(path[0]=='/') {
		strcpy(dirname, "/");
	} else {
		strcpy(dirname, "");
	}
	for(x=0;x<num_splits-1;x++) {
		strcat(dirname, temp_path_split[x]);
		if(x<num_splits-2) {
			strcat(dirname, "/");
		}
	}

	for(x=0;x<num_splits;x++) {
		free(temp_path_split[x]);
	}
	free(temp_path_split);
	return TRUE;
}

int get_duration(const char *filename, short unsigned int *duration) {
	if(file_exists(filename)) {
		int time[3]={0, 0, 0}, time_counter=0;
		char *command=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
		char *line=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
		command[0]='\0';
		line[0]='\0';
		*duration=0;

		strcat(command, "bin/ffmpeg -i \"");
		strcat(command, filename);
		strcat(command, "\" 2>&1");

		FILE *p=popen(command, "r");
		while(fgets(line, MAX_STRING_LEN, p)!=NULL) {
			if(contains_str(line, "Duration")) {
				char full_str[MAX_STRING_LEN];
				char duration_str[MAX_STRING_LEN];
				if(split_n_z(line, full_str, ',', 0) && split_n_z(full_str, duration_str, ' ', 1)) {
					int x, time_counter=0;
					for(x=0;x<strlen(duration_str) && duration_str[x]!='.';x++) {
						if(duration_str[x]==':') {
							time_counter++;
						} else {
							time[time_counter]*=10;
							time[time_counter]+=duration_str[x]-'0';
						}
					}
					*duration=time[0]*60*60+time[1]*60+time[2];
					break;
				}
			}
			line[0]='\0';
		}

		pclose(p);

		free(line);
		free(command);
	}
	if(*duration>0) {
		return TRUE;
	}
	return FALSE;
}

int get_fps(const char *filename, float *fps) {
	int x;
	//char **exec_args;
	*fps=-1;
	if(file_exists(filename)) {
		char command[MAX_STRING_LEN];
		char *line=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
		command[0]='\0';
		line[0]='\0';
		*fps=-1;

		strcat(command, "bin/ffmpeg -i \"");
		strcat(command, filename);
		strcat(command, "\" 2>&1");


		FILE *p=popen(command, "r");
		while(fgets(line, MAX_STRING_LEN, p)!=NULL) {
			if(contains_str(line, "Video") && contains_str(line, "Stream") && contains_str(line, "fps")) {
				int count=0;
					if(!split_count(line, ',', &count) || count==0) {
						continue;
					}
					char **full_str;
					char fps_str[MAX_STRING_LEN];

					full_str=(char **)malloc(sizeof(char *)*count);
					for(x=0;x<count;x++) {
						full_str[x]=(char *)malloc(sizeof(char)*MAX_STRING_LEN);
					}

					if(split(line, full_str, ',')) {
						for(x=0;x<count;x++) {
							if(contains_str(full_str[x], "fps") && split_n_z(full_str[x], fps_str, ' ', 0)) {
								*fps=atof(fps_str);
								break;
							}
						}
					}
					for(x=0;x<count;x++) {
						free(full_str[x]);
					}
					free(full_str);
					if(*fps>0) {
						break;
					}
			}
		}

		pclose(p);

	}
	if(*fps>0) {
		return TRUE;
	}
	return FALSE;
}

int subs_exist(const char *filename) {
	if(file_exists(filename)) {
		char command[MAX_STRING_LEN], line[MAX_STRING_LEN];
		command[0]='\0';
		line[0]='\0';

		strcat(command, "bin/ffmpeg -i \"");
		strcat(command, filename);
		strcat(command, "\" 2>&1");

		FILE *p = popen(command, "r");

		while(fgets(line, MAX_STRING_LEN, p)!=NULL) {
			if(contains_str(line, "Stream") && contains_str(line, "Subtitle")) {
				char full_str[MAX_STRING_LEN], subtitle_str[MAX_STRING_LEN];
				if(split_n_z(line, full_str, ':', 3) && split_n_z(full_str, subtitle_str, ' ', 0)) {
					break;
				}
			}
		}
		pclose(p);
	}
	return FALSE;
}
