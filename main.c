#include <stdio.h>
#include <stdlib.h>

#include "core/engine.h"

int main(int argc, char *argv[]) {
	if(argc==1 || argc > MAX_ARGS) {
		fprintf(stdout, "ERROR: Not enough arguments!\n");
		free_globals();
		exit(EXIT_FAILURE);
	}
	if(!init_globals()) {
		fprintf(stderr, "ERROR: init_globals returned FALSE!\n");
		free_globals();
		exit(EXIT_FAILURE);
	}
	if(!validate_params(argc, (const char **)argv)) {
		fprintf(stderr, "ERROR: validate_params returned FALSE!\n");
		free_globals();
		exit(EXIT_FAILURE);
	}
	if(!preprocess()) {
		fprintf(stderr, "ERROR: preprocess returned FALSE!\n");
		free_globals();
		exit(EXIT_FAILURE);
	}
	if(!encode()) {
		fprintf(stderr, "ERROR: encode returned FALSE!\n");
		free_globals();
		exit(EXIT_FAILURE);
	}
	free_globals();
	exit(EXIT_SUCCESS);
}
