encoder
=======
What is it? 
-------
encoder.exe is a small application used (in conjunction with ffmpeg.exe) to batch encode video files located within a directory and/or subdirectories. 

How do I use it?
-------
First ensure that ffmpeg.exe is located in a folder called bin (which should be in the same directory as encoder.exe).

- src
    This is the source file, or source directory that will be scanned. 
    NOTE: Individual files cannot be entered into the application as a source directory. 
    
        eg. encoder.exe src=files

- dst
    This is the destination directory. If it is specified, the location will be used as the destination directory. Otherwise, the default location of src/encodes will be used.
    NOTE: Individual files cannot be entered into the application as a destination directory. 
    
        eg. encoder.exe src=files dst=files/encodes

- quality = HD | SD | (int)
    There are 2 quality profiles stored within the application. HD will attempt to lower the quality, in an effort to preserve file space, while SD will attempt to increase the quality in exchange for increased file space.
    
        eg. encoder.exe quality=HD src=file.mkv dst=file.mp4

	NOTE: THIS IS NOT SUPPORTED AT THE MOMENT

- recursive
    This option will scan a directory recursively, select any valid files which have not been encoded already, and encode the selected files.
    
        eg. encoder.exe src=directory recursive

        directory
        directory/file1.avi
        directory/file2.avi
        directory/director2/file3.mkv
        directory/director3/file4.mkv

        The list of files that will be encoded will be the following: 
        - file1.avi
        - file2.avi
        - file3.mkv
        - file4.mkv

- no-subs
    By default, subtitle track #1 will be encoded as a hardsub on the encoded video. The no-subs option is used in order to avoid encoded subtitles.
    
        eg. encoder.exe src=<filename or directory> no-subs